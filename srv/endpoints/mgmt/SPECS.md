Red Hat JBoss BRMS Suite 6.0.2 cartridge installed.  Please make note of these credentials:

             BRMS Admin User: brms-admin
                    Password: 3MHeDeBBwxLG

           BRMS Analyst User: brms-analyst
                    Password: TAVEujprsStS


Configuration files:
- You can add/modify/show/remove users and roles using the following files in your cartridge git repository:
   - Users -> .openshift/config/brms-users.properties
   - Roles -> .openshift/config/brms-roles.properties
2.- You can modify the JBoss EAP standalone configuration file used for the BRMS application in your cartridge git repository at path .openshift/config/standalone.xml
3.- You can modify the Maven settings configuration file used for the BRMS application in your cartridge git repository at path .openshift/config/settings.xml
20150805-02:51:31
20150805-02:51:31
20150805-02:51:31
20150805-02:51:39
20150805-02:51:39
