#!/bin/bash

export PREVIOUS_LOCATION=$DIRSTACK

cd ~/hive/mongos

mongorestore dump/octopus
mongorestore dump/vortex

cd $PREVIOUS_LOCATION

unset PREVIOUS_LOCATION
